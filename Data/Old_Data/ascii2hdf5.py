import numpy as np
import codecs
import h5py

file_name = "alpha1d"

# 1D
'''
with codecs.open(file_name + '.dat', encoding='utf-8-sig') as f:
	X = [[float(x) for x in line.split()] for line in f]


data = np.array(X)[:,1]
coord = np.array(X)[:,0]

hdf = h5py.File(file_name + '.h5', mode='w')
dset = hdf.require_dataset('data', data.shape, dtype='f')
dset[:] = data[:]

dsetC = hdf.require_dataset('coord1', data.shape, dtype='f')
dsetC[:] = coord[:]

hdf.close()
'''
# 2D
with codecs.open('t_Q_0p1_M_1p30_R_9p94v2.dat', encoding='utf-8-sig') as f:
    X = [[float(x) for x in line.split()] for line in f]

hdf = h5py.File('data.h5', mode='w')

data = np.array(X).astype(np.float16)

xcoord = np.unique(data[:, 0])
ycoord = np.unique(data[:, 1])

values = np.zeros((xcoord.shape[0], ycoord.shape[0]))

for (i,j), v in np.ndenumerate(values):
    values[i,j] = data[np.where(((data[:, 0] == xcoord[i]) & (data[:, 1] == ycoord[j])))[0]][:,2]
dset = hdf.require_dataset('Alpha', values.shape, dtype='f')
dset[:] = values[:]

for (i,j), v in np.ndenumerate(values):
    values[i,j] = data[np.where(((data[:, 0] == xcoord[i]) & (data[:, 1] == ycoord[j])))[0]][:,3]
dset = hdf.require_dataset('grr', values.shape, dtype='f')
dset[:] = values[:]

for (i,j), v in np.ndenumerate(values):
    values[i,j] = data[np.where(((data[:, 0] == xcoord[i]) & (data[:, 1] == ycoord[j])))[0]][:,4]
dset = hdf.require_dataset('p', values.shape, dtype='f')
dset[:] = values[:]

for (i,j), v in np.ndenumerate(values):
    values[i,j] = data[np.where(((data[:, 0] == xcoord[i]) & (data[:, 1] == ycoord[j])))[0]][:,5]
dset = hdf.require_dataset('rho', values.shape, dtype='f')
dset[:] = values[:]

dsetC1 = hdf.require_dataset('coord1', xcoord.shape, dtype='f')
dsetC1[:] = xcoord[:]

dsetC2 = hdf.require_dataset('coord2', ycoord.shape, dtype='f')
dsetC2[:] = ycoord[:]


hdf.close()

# 3D
'''
with codecs.open('phiRF4.0.0.000.dat', encoding='utf-8-sig') as f:
	X = [[float(x) for x in line.split()] for line in f]


data = np.array(X)

xcoord = np.unique(data[:, 0])
ycoord = np.unique(data[:, 1])
zcoord = np.unique(data[:, 2])

values = np.zeros((xcoord.shape[0], ycoord.shape[0], zcoord.shape[0]))

for (i,j,k), v in np.ndenumerate(values):
	values[i,j,k] = data[np.where(((data[:, 0] == xcoord[i]) & (data[:, 1] == ycoord[j]) & (data[:, 2] == zcoord[k])))[0]][:,3]

hdf = h5py.File('phir3D.h5', mode='w')
dset = hdf.require_dataset('data', values.shape, dtype='f')
dset[:] = values[:]

dsetC1 = hdf.require_dataset('coord1', xcoord.shape, dtype='f')
dsetC1[:] = xcoord[:]

dsetC2 = hdf.require_dataset('coord2', ycoord.shape, dtype='f')
dsetC2[:] = ycoord[:]

dsetC3 = hdf.require_dataset('coord3', zcoord.shape, dtype='f')
dsetC3[:] = zcoord[:]

hdf.close()

'''
