import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset


def calcula_integral2D(folder, field, axis):
    field = readData(folder, field)

    #dx = 2*10.0/100.
    dx = 0.125

    acc = 0
    for i in range(field.shape[axis]):
        x = -10 + i * dx
        if axis == 0:
            acc = acc + x**2 * np.sum(field[i, :]) * dx**2
        if axis == 1:
            acc = acc + x**2 * np.sum(field[:, i]) * dx**2
    return acc

def calcula_integral3D(folder, field, axis):
    field = readData(folder, field)

    dx = 2*10.0/100.

    acc = 0
    for i in range(field.shape[axis]):
        x = -10 + i * dx
        if axis == 0:
            acc = acc + x**2 * np.sum(field[i, :, :]) * dx**3
        if axis == 1:
            acc = acc + x**2 * np.sum(field[:, i, :]) * dx**3
        if axis == 2:
            acc = acc + x**2 * np.sum(field[:, :, i]) * dx**3            
    return acc

def main():

    max_iter = 6000
    delta_iter = 10
    dt = 0.5

    #Range limit
    max_iter = max_iter + 1
    
    data_q0_xx = np.zeros((1 + max_iter//delta_iter))
    data_q0_yy = np.zeros((1 + max_iter//delta_iter))

    data_q1_xx = np.zeros((1 + max_iter//delta_iter))
    data_q1_yy = np.zeros((1 + max_iter//delta_iter))

    data_q1p_xx = np.zeros((1 + max_iter//delta_iter))
    data_q1p_yy = np.zeros((1 + max_iter//delta_iter))


    for t in xrange(0, max_iter, delta_iter):
        print max_iter//delta_iter, t, t//delta_iter
        data_q0_xx[t//delta_iter] = calcula_integral2D("./newQ05/outputDir_slice0/visit_dump." + str(t).zfill(5) + "/rhof_3.hdf5", "rhof", 0)
        data_q0_yy[t//delta_iter] = calcula_integral2D("./newQ05/outputDir_slice0/visit_dump." + str(t).zfill(5) + "/rhof_3.hdf5", "rhof", 1)

        data_q1_xx[t//delta_iter] = calcula_integral2D("./newQ1/outputDir_slice0/visit_dump." + str(t).zfill(5) + "/rhof_3.hdf5", "rhof", 0)
        data_q1_yy[t//delta_iter] = calcula_integral2D("./newQ1/outputDir_slice0/visit_dump." + str(t).zfill(5) + "/rhof_3.hdf5", "rhof", 1)

#        data_q1p_xx[t//delta_iter] = calcula_integral2D("./Q1p/outputDir_slice0/visit_dump." + str(t).zfill(5) + "/rhof_3.hdf5", "rhof", 0)
#        data_q1p_yy[t//delta_iter] = calcula_integral2D("./Q1p/outputDir_slice0/visit_dump." + str(t).zfill(5) + "/rhof_3.hdf5", "rhof", 1)
    

    time = np.arange(0, max_iter, delta_iter) * dt / delta_iter

    np.savetxt('q05.out', np.column_stack((time,(data_q0_xx - data_q0_yy)/(data_q0_xx + data_q0_yy))))
    np.savetxt('q1.out', np.column_stack((time,(data_q1_xx - data_q1_yy)/(data_q1_xx + data_q1_yy))))

    plt.figure()
    plt.title('Diff integral')
#    plt.plot(time, abs(data_q0_xx - data_q0_yy)/abs(data_q0_xx + data_q0_yy), label='Q0', linestyle="-")
#    plt.plot(time, abs(data_q1_xx - data_q1_yy)/abs(data_q1_xx + data_q1_yy), label='Q1', linestyle="-")
#    plt.plot(time, abs(data_q0_xx - data_q0_yy), label='Q0', linestyle="-")
#    plt.plot(time, abs(data_q1_xx - data_q1_yy), label='abs(Q1)', linestyle="-")
    plt.plot(time, (data_q1_xx - data_q1_yy)/(data_q1_xx + data_q1_yy), label='Q1', linestyle="-")
#    plt.plot(time, -0.95*(data_q1p_xx - data_q1p_yy)/(data_q1p_xx + data_q1p_yy), label='Q1p', linestyle="-")
    plt.plot(time, (data_q0_xx - data_q0_yy)/(data_q0_xx + data_q0_yy), label='Q05', linestyle="-")    
#    plt.plot(time, data_q1_xx, label='Q1xx', linestyle="-")
#    plt.plot(time, data_q1_yy, label='Q1yy', linestyle="-")    
    plt.legend(loc = 2)
#    plt.ylim(bottom=-0.003)
#    plt.ylim(top=-0.0024)
    plt.xlim(right=10)
    plt.savefig('asymmetry_new_zoom.png')

if __name__ == "__main__":
    main()
